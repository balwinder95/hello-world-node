var express = require('express');
var router = express.Router();

/* GET test page. */
router.get('/', function (req, res) {
  res.end("Hello World!");
});

module.exports = router;