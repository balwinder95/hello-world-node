var express = require('express');
var app = express();
var port = require('./config/localDevelopment').PORT;
var routes = require('./routes/index');

app.get('/', routes);

var server = app.listen(port, function () {
  console.log("Express server listening on port " + port);
});